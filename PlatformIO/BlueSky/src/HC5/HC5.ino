/*
 * Version fonctionnant sur Arduino NANO
 * Monitor en 38400.
 */


#include <Arduino.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(4, 5); // RX, TX

void setup() {
  Serial.begin(38400);
  delay(300);
  pinMode(2,OUTPUT); digitalWrite(2,HIGH);
  Serial.println("COMMAND AT:");
  mySerial.begin(38400);
}

void loop()
{

 if (mySerial.available()) 
  {
    Serial.write(mySerial.read()); 
  }
  
  if (Serial.available())
  {
    mySerial.write(Serial.read());
  }
  
}